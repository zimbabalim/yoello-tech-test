/**
 * Global configuration properties
 */
const config = {
  // *** Api properties
  api: {
    origin: 'https://api.punkapi.com/v2/beers',
    requests: {
      all: {
        type: 'all',
        label: 'All',
        path: ''
      },
      steak: {
        type: 'steak',
        label: 'Steak',
        path: '?food=steak'
      },
      pizza: {
        type: 'pizza',
        label: 'Pizza',
        path: '?food=pizza'
      }
    }
  },
  
  // *** Any default values required go here
  defaults: {
    productType: 'all',
    sorting: {
      key: 'name',
      mode: 'ascending'
    }
  }
}

export default config;
