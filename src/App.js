import React from 'react';
import {StateProvider} from './state';
import store from './state/store';
import Layout from './components/Layout';
import './scss/App.scss';

/**
 * Entry point.
 * Note <StateProvider> provides state management to all children
 * @returns {*}
 * @constructor
 */
const App = () => {
  
  return (
      <StateProvider initialState={store.initialState} reducer={store.reducer}>
        <Layout/>
      </StateProvider>
  )
}

export default App;
