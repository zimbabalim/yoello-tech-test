import actions from './actions';
import config from '../config';
import utils from './utils';

/**
 * Data store and reducer, processing offloaded to `utils`
 */
const store = {
  initialState: {
    
    productData: {
      all: null,
      steak: null,
      pizza: null,
    },
    
    productDataCombined: null, // *** all product data subsets combined for filtering ops
    totalNumberOfProducts: 0,
    
    shoppingCartIds: [], // *** scratch for simple add/delete ops
    shoppingCartData: [], // *** added product data
    shoppingCartTotalPrice: 0,
    shoppingCartIsFocused: false, // *** expand / collapse state
    
    productTypesLoaded: [], // *** enable components to use simple lookup
    currentProductType: config.defaults.productType, // *** 'all' || 'steak' || 'pizza'
    currentViewSwitcherIndex: 0,
    selectedProductItemData: null,
    
    sortingValues: null
  },
  
  reducer: (state, action) => {
    
    switch (action.type) {
      
      // *** data is fetched on request, so process and add to the store on arrival
      case actions.SET_PRODUCT_RESPONSE:
        
        // *** sort either by default or user set values if present
        utils.sortBy(
            action.payload.data,
            (state.sortingValues) ? state.sortingValues.key : config.defaults.sorting.key,
            (state.sortingValues) ? state.sortingValues.mode : config.defaults.sorting.mode
        );
        
        const productData = Object.assign({}, state.productData);
        productData[action.payload.productType] = action.payload.data;
        
        const productTypesLoaded = [...state.productTypesLoaded];
        productTypesLoaded.push(action.payload.productType);
        
        let totalNumberOfProducts = state.totalNumberOfProducts;
        totalNumberOfProducts += action.payload.data.length;
        
        
        return {
          ...state,
          productData,
          productTypesLoaded,
          totalNumberOfProducts
        };
      
        // *** on navigation change
      case actions.SET_CURRENT_PRODUCT_TYPE:
        return {
          ...state,
          currentProductType: action.payload.productType,
          currentViewSwitcherIndex: action.payload.index
        };
      
      case actions.PRODUCT_ITEM_SELECTED:
        
        let selectedProductItemData = null;
        
        if (action.payload) { // *** otherwise clear data and component will unmount
          selectedProductItemData = utils.getProductByIndex(
              state.productData[action.payload.productType],
              action.payload.index
          );
        }
        
        utils.uiToggleScrollLock(!!action.payload);
        
        return {
          ...state,
          selectedProductItemData
        };
        
        // *** ADD item to shopping cart
      case actions.SHOPPING_CART_ADD_ITEM:
        
        const mergedData = utils.mergeData(
            state.productData,
            state.productDataCombined,
            state.totalNumberOfProducts
        );
        
        const addedIds = utils.addItemToShoppingCart(
            [...state.shoppingCartIds],
            action.payload.id
        );
        
        const addedData = utils.updateShoppingCartData(
            mergedData,
            addedIds
        );
        
        return {
          ...state,
          productDataCombined: mergedData,
          shoppingCartIds: addedIds,
          shoppingCartData: addedData,
          shoppingCartTotalPrice: utils.calculatePrice(addedData)
        };
        
        // *** DELETE item from shopping cart
      case actions.SHOPPING_CART_REMOVE_ITEM:
        
        const newIds = utils.removeItemFromShoppingCart(state.shoppingCartIds, action.payload.id);
        const newData = utils.updateShoppingCartData(
            state.productDataCombined,
            newIds
        );
        
        return {
          ...state,
          shoppingCartIds: newIds,
          shoppingCartData: newData,
          shoppingCartTotalPrice: utils.calculatePrice(newData)
        };
      
        // *** on expand / collapse
      case actions.SHOPPING_CART_FOCUS:
        utils.uiToggleScrollLock(action.payload.isFocused);
        return {
          ...state,
          shoppingCartIsFocused: action.payload.isFocused
        };
      
      
      case actions.SET_SORTING:
        let productDataTemp = Object.assign({}, state.productData); // *** productData
        
        const sorted = utils.sortAll(
            productDataTemp,
            action.payload.key,
            action.payload.mode
        )
        
        return {
          ...state,
          productData: sorted,
          sortingValues: action.payload
        };
        
      
      default:
        return state;
    }
  }
};

export default store;
