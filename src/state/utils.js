/**
 * Pure functional methods to process reducer requirements
 */
const utils = {
  
  getProductByIndex: (dataset, index) => {
    return dataset[index];
  },
  
  addItemToShoppingCart: (shoppingCartIds, id) => {
    if (!shoppingCartIds.includes(id)) {
      shoppingCartIds.push(id);
    }
    return shoppingCartIds;
  },
  
  removeItemFromShoppingCart: (shoppingCartIds, id) => {
    shoppingCartIds = shoppingCartIds.filter(
        (item) => item !== id
    );
    return shoppingCartIds;
  },
  
  // ***
  updateShoppingCartData: (data, ids) => {
    const output = ids.reduce(
        (result, id) => {
          const matches = data.find((item) => {
            return item.id === id;
          });
          return result.concat(matches);
        }, []
    );
    return [...new Set(output)]; // *** unique'd
  },
  
  calculatePrice: (data) => {
    let total = 0;
    
    data.map((item) => {
      return total += item.abv;
    });
    
    return Math.round((total + Number.EPSILON) * 100) / 100;
  },
  
  /**
   * flatten contents of all available product data
   * @param data {Object} : product data
   * @param mergedData {Array} : previous result of this function
   * @param total {Number} : item count
   * @return {*}
   */
  mergeData: (data, mergedData, total) => {
    // *** if true just return without operations below
    if (mergedData && (mergedData.length === total)) {
      return mergedData;
    }
    
    const toArray = Object.entries(data).map((item) => {
      return item;
    });
    
    return toArray.reduce(
        (result, item) => {
          const target = item[1] || null;
          if (target) {
            return result.concat(target);
          }
          return result;
        }, []
    );
  },
  
  sortAll: (data, key, mode) => {
    Object.entries(data).map((item) => {
      let target = data[item[0]];
      if (target) {
        target = utils.sortBy(
            target,
            key,
            mode
        )
      }
      return null;
    });
    
    return data;
  },
  
  sortBy: (data, key, mode) => {
    const direction = (mode === 'ascending') ? [-1, 1] : [1, -1];
    
    return data.sort((a, b) => {
      let res = 0;
      if (a[key] !== b[key]) {
        res = a[key] < b[key] ? direction[0] : direction[1];
      }
      return res;
    });
  },
  
  // *** enable / disable scrolling when modal type components are mounted / activated
  uiToggleScrollLock: (bool) => {
    if (bool) {
      document.body.classList.add('scroll-lock');
    } else {
      document.body.classList.remove('scroll-lock');
    }
  }
}

export default utils;
