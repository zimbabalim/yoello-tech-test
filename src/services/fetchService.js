import axios from 'axios';

/**
 * Wrapper for axios fetch.
 * Accepts url and returns response to caller via callback
 */
const fetchService = {
  
  /**
   *
   * @param request {String}
   * @param onCompleteFn {Function}
   * @return {Promise<unknown>}
   */
  call: (request, onCompleteFn) => {

    return axios
        .get(request)
        .then((response) => {
          onCompleteFn(
              {status: 'ok', payload: response.data}
          );
          return true;
        })
        .catch((error) => {
          console.error('/fetchService/ -ERROR', error);
          onCompleteFn(
              {status: 'fail', payload: null}
          );
        })
        .finally(() => {
          //
        });
  }
}

export default fetchService;
