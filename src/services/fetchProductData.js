import fetchService from './fetchService';
import actions from '../state/actions';
import config from '../config';

const fetchProductData = (vo) => {
  
  // *** make request
  const request = () => {
    fetchService.call(
        `${config.api.origin}${config.api.requests[vo.productType].path}`,
        (result) => {
          if (result.payload) {
            vo.dispatch({
              type: actions.SET_PRODUCT_RESPONSE,
              payload: {
                productType: vo.productType,
                data: result.payload
              }
            })
            // *** success
          } else {
            // *** handle error
          }
        }
    )
  }
  
  // *** determine if call should be made
  if (vo.productType === vo.currentProductType) {
    if (!vo.productTypesLoaded.includes(vo.productType)) {
      // ******
      request();
      // ******
    } else {
      // *** do nothing, has data
    }
  }
}

export default fetchProductData;
