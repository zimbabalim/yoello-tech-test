import React from 'react';
import ViewSwitcher from './ViewSwitcher';
import ProductDetail from './product-detail/ProductDetail';
import ShoppingCart from './shopping-cart/ShoppingCart';

const Layout = (props) => {
  
  return (
      <div className={'viewport'}>
      
        <ViewSwitcher/>
        <ProductDetail/>
        <ShoppingCart/>
    
      </div>
  )
}

export default Layout;
