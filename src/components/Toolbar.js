import React, {useEffect, useState} from 'react';
import {useStateValue} from '../state';
import actions from '../state/actions';
import config from '../config';
import './toolbar.scss';

const Toolbar = (props) => {
  
  const [{currentViewSwitcherIndex}, dispatch] = useStateValue();
  const [currentSortKey, setCurrentSortKey] = useState(config.defaults.sorting.key);
  const [currentSortMode, setCurrentSortMode] = useState(config.defaults.sorting.mode);
  
  useEffect(() => {
    dispatch(
        {type: actions.SET_SORTING,
          payload: {
            key: currentSortKey,
            mode: currentSortMode
          }
        });
  }, [currentSortKey, currentSortMode]); // eslint-disable-line
  
  const renderNavButtons = () => {
    
    return Object.entries(config.api.requests).map((item, index) => {
      return (
          <button
              key={`toolbar__nav-button${index}`}
              className={
                `f__button toolbar__nav-button
                ${currentViewSwitcherIndex === index ?
                    'toolbar__nav-button--is-active' : ''}`}
              onClick={() => {
                dispatch(
                    {type: actions.SET_CURRENT_PRODUCT_TYPE,
                      payload: {productType: item[1].type, index}}
                )
              }}>
            {item[1].label}
          </button>
      )
    })
  }
  
  const renderSortButton = (label, type, value) => {
    return (
        <button
            className={
              `f__button toolbar__sort-button
                ${(currentSortKey === value || currentSortMode === value) ?
                  'toolbar__sort-button--is-active' : ''}`}
            onClick={() => {
              if (type === 'key') {
                setCurrentSortKey(value);
              }
              if (type === 'mode') {
                setCurrentSortMode(value);
              }}}
        >
          {label}
        </button>
    )
  }
  
  return (
      <div className={'toolbar'}>
        <div className="toolbar__inner">
          
          <div className={'toolbar__nav-btns'}>
            {renderNavButtons()}
          </div>
          
          <div className="toolbar__sort-btns">
            <div className="toolbar--btn-group">
              {renderSortButton('Name', 'key', 'name')}
              {renderSortButton('ABV', 'key', 'abv')}
            </div>
            
            <div className="toolbar--btn-group">
              {renderSortButton('Ascending', 'mode', 'ascending')}
              {renderSortButton('Descending', 'mode', 'descending')}
            </div>
          </div>
        
        </div>
      
      </div>
  )
}

export default Toolbar;
