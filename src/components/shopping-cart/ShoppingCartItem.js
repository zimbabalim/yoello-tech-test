import React from 'react';
import './shopping-cart.scss';

const ShoppingCartItem = (props) => {
  
  const {name, id, image, price, onRemoveFn} = props;
  
  return (
      <div className={'shopping-cart-item'}>
        
        <div className={'shopping-cart-item__image-wrapper'}>
          <img
              className={'shopping-cart-item__image'}
              src={image}
              alt={{}} />
        </div>
        
        <div className="shopping-cart-item__text">
          
          <span className={'shopping-cart-item__text--label'}>{name}</span>
          <span className={'shopping-cart-item__text--price'}>£{price}</span>

        </div>
        
        <button
            className={'f__button f__button--secondary shopping-cart-item__remove-btn'}
            onClick={() => {onRemoveFn(id)}}>
          REMOVE
        </button>
      
      </div>
  )
}

export default ShoppingCartItem;
