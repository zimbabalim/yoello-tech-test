import React, {useEffect, useRef, useState} from 'react';
import {useStateValue} from '../../state';
import actions from '../../state/actions';
import './shopping-cart.scss';
import ShoppingCartItem from './ShoppingCartItem';


const ShoppingCart = (props) => {
  
  const [{shoppingCartData, shoppingCartTotalPrice}, dispatch] = useStateValue();
  
  const [isExpanded, setIsExpanded] = useState(false);
  const [headerHeight, setHeaderHeight] = useState(null);
  const [innerHeight, setInnerHeight] = useState(0);
  
  const [yPos, setYPos] = useState(0);
  
  const headerRef = useRef(null);
  const innerRef = useRef(null);
  
  // *** on expand / collapse
  useEffect(() => {
    
    if (!isExpanded) {
      setYPos(headerHeight)
    }
    if (isExpanded) {
      setYPos(innerHeight + headerHeight)
    }
    
    dispatch({
      type: actions.SHOPPING_CART_FOCUS,
      payload: {isFocused: isExpanded}
    })
    
  }, [isExpanded]); // eslint-disable-line
  
  
  // *** init
  useEffect(() => {
    
    const height = headerRef.current.getBoundingClientRect().height;
    setHeaderHeight(height);
    setYPos(height);
    
    setInnerHeight(
        innerRef.current.getBoundingClientRect().height
    )
  }, [props]);

  
  const removeItem = (id) => {
    dispatch({
      type: actions.SHOPPING_CART_REMOVE_ITEM,
      payload: {id}
    })
  }
  
  
  const renderItems = () => {
    
    if (!shoppingCartData) {
      return null;
    }
    
    return shoppingCartData.map((item, index) => {
      return (
          <ShoppingCartItem
              key={`ShoppingCartItem${index}`}
              name={item.name}
              id={item.id}
              image={item.image_url}
              price={item.abv}
              onRemoveFn={removeItem}
          />
      )
    })
  }
  
  return (
      <div className={'shopping-cart'} style={{top: `calc(100vh - ${yPos}px`}}>
        
        <div
            className={`shopping-cart__glass ${isExpanded ? 'shopping-cart__glass--is-expanded' : ''}`}
            onClick={() => {
              setIsExpanded(!isExpanded)
            }}
        />
        
        <div
            ref={headerRef}
            className={'shopping-cart__header'}
            onClick={() => {
              setIsExpanded(!isExpanded)
            }}
        >
          
          <h3 className={'shopping-cart__title'}>Shopping cart</h3>
          
          {shoppingCartData.length > 0  && (
              <>
                <span>Items: {shoppingCartData.length}</span>
                <span>Total: £{shoppingCartTotalPrice}</span>
              </>
          )}
        
        </div>
        
        <div
            ref={innerRef}
            className="shopping-cart__inner">
          
          <div className="shopping-cart__scroll-area">
            {shoppingCartData.length > 0 ?
                renderItems()
                :
                <p className={'shopping-cart__empty-msg'}>Cart is empty</p>
            }
          </div>
        
        </div>
      
      </div>
  )
}

export default ShoppingCart;
