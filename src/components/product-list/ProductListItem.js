import React from 'react';

const ProductListItem = (props) => {
  
  const {index, name, abv, image, onSelectedFn} = props;
  
  return (
      <div className={'product-item'} onClick={() => onSelectedFn(index)}>
        
        <div className={'product-item__image-wrapper'}>
          <img src={image} alt={name} className={'product-item__image'}/>
        </div>
        
        <div className="product-item__text">
          <p className={'product-item__line-1'}>{name}</p>
          <p className={'product-item__line-2'}>{abv}</p>
        </div>
      
      </div>
  )
}

export default ProductListItem;
