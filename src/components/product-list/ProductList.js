import React, {useEffect, useState} from 'react';
import {useStateValue} from '../../state';
import actions from '../../state/actions';
import fetchProductData from '../../services/fetchProductData';
import ProductListItem from './ProductListItem';
import './product.scss';

const ProductList = (props) => {
  
  const [{currentProductType, productTypesLoaded, productData}, dispatch] = useStateValue();
  const [dataset, setDataset] = useState(null);
  const {productType} = props;
  
  // *** fetch product data (only once, and if required - helper method handles this)
  useEffect(() => {

    fetchProductData({
      productType,
      currentProductType,
      productTypesLoaded,
      dispatch
    });
  }, [currentProductType]); // eslint-disable-line
  
  // *** populate dataset state from store
  useEffect(() => {
    const data = productData[productType];
    if (data) {
      setDataset(data);
    }
  }, [productData]); // eslint-disable-line
  
  const onItemSelected = (index) => {
    
    dispatch({
      type: actions.PRODUCT_ITEM_SELECTED,
      payload: {productType, index}
    })
  }
  
  return (
      <div className={'product-list'}>
        
        <div className="product-list__inner">
          {dataset && dataset.map((item, index) => {
            return (
                <ProductListItem
                    key={`ProductListItem${index}`}
                    index={index}
                    name={item.name}
                    abv={item.abv}
                    image={item.image_url}
                    onSelectedFn={onItemSelected}
                />
            )
          })}
          
          {!dataset && (
              <div className={'product-list__spinner'}>
                LOADING...
              </div>
          )}
          
        </div>
      
      </div>
  )
}

export default ProductList;
