import React, {useEffect, useState} from 'react';
import {useStateValue} from '../../state';
import './product-detail.scss';
import actions from '../../state/actions';

const ProductDetail = (props) => {
  
  const [{selectedProductItemData, shoppingCartIds}, dispatch] = useStateValue();
  const [isAlreadyInCart, setIsAlreadyInCart] = useState(false);
  const [yPos, setYPos] = useState(0);
  
  useEffect(() => {
    if (!selectedProductItemData || !shoppingCartIds) {
      return;
    }
    setIsAlreadyInCart(
        shoppingCartIds.includes(selectedProductItemData.id)
    );
    
    setYPos(window.scrollY);
    
  }, [shoppingCartIds, selectedProductItemData]);
  
  const closeOverlay = () => {
    dispatch({
      type: actions.PRODUCT_ITEM_SELECTED,
      payload: null
    })
  }
  
  const addItemToShoppingCart = (shouldAdd = true) => {
    dispatch({
      type: (shouldAdd) ?
          actions.SHOPPING_CART_ADD_ITEM :
          actions.SHOPPING_CART_REMOVE_ITEM,
      payload: {id: selectedProductItemData.id}
    })
  }
  
  const splitFoodPairings = () => {
    return selectedProductItemData.food_pairing.map((item, index) => {
      return (
          <span
              key={`product-detail__food-item${index}`}
              className={'product-detail__food-item'}>
            {item}
          </span>
      )
    });
  }
  
  return (
      <>
        {selectedProductItemData && (
            <div
                className={'product-detail'}
                style={{top: `${yPos}px`}}>
              
              <div
                  className="product-detail__glass"
                  onClick={() => closeOverlay()} />
              
              <div className="product-detail__inner">
                <button
                    className={'f__button product-detail__close-btn'}
                    onClick={() => closeOverlay()}>
                  X
                </button>
                
                <div className={'product-detail__text'}>
                  <h2>{selectedProductItemData.name}</h2>
                  <p>{selectedProductItemData.tagline}</p>
                  <p>ABV: {selectedProductItemData.abv}</p>
                  <p>{selectedProductItemData.description}</p>
                  
                  <div className={'product-detail__food-pairings'}>
                    {splitFoodPairings()}
                  </div>
                  
                </div>
                
                <div className={'product-detail__image-wrapper'}>
                  <img
                      className={'product-detail__image'}
                      src={selectedProductItemData.image_url}
                      alt={selectedProductItemData.name} />
                </div>
                
                {!isAlreadyInCart ? (
                        <button
                            className={'f__button f__button--primary product-detail__cart-btn'}
                            onClick={() => addItemToShoppingCart(true)}>
                          ADD TO BASKET
                        </button>
                    )
                    :
                    (
                        <button
                            className={'f__button f__button--secondary product-detail__cart-btn'}
                            onClick={() => addItemToShoppingCart(false)}>
                          REMOVE FROM BASKET
                        </button>
                    )
                }
              
              
              </div>
            </div>
        )}
      </>
  )
}

export default ProductDetail;
