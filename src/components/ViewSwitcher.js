import React, {useEffect, useState, useRef} from 'react';
import {useStateValue} from '../state';
import Toolbar from './Toolbar';
import './view-switcher.scss';
import config from '../config';
import ProductList from './product-list/ProductList';
import Hammerjs from 'hammerjs';
import actions from '../state/actions';

const ViewSwitcher = (props) => {
  
  const [{currentViewSwitcherIndex}, dispatch] = useStateValue();
  const [numPanels, ] = useState(Object.keys(config.api.requests).length);
  const [hasSwiped, setHasSwiped] = useState(null);
  
  const elRef = useRef(null);
  
  useEffect(() => {
    const hammer = new Hammerjs(elRef.current);
    hammer.on('swipeleft', () => {
      setHasSwiped('left');
    });
    hammer.on('swiperight', () => {
      setHasSwiped('right');
    });
    
  }, [props]);
  
  useEffect(() => {
    onSwipe(hasSwiped);
  }, [hasSwiped]); // eslint-disable-line
  
  
  const onSwipe = (direction) => {
  
    let types = Object.keys(config.api.requests).map((item) => {
      return item;
    });
    setHasSwiped(null);
    
    let index = currentViewSwitcherIndex;
  
    if (direction === 'left') {
      index = index + 1;
    }
    if (direction === 'right') {
      index = index - 1;
    }
    if (index <= 0) {
      index = 0;
    }
    if (index >= types.length - 1) {
      index = types.length - 1;
    }
    
    dispatch(
        {type: actions.SET_CURRENT_PRODUCT_TYPE,
          payload: {productType: types[index], index}}
    )
  }
  
  const renderProductLists = () => {
    
    return Object.keys(config.api.requests).map((item, index) => {
      return (
          <ProductList
              key={`ProductList${index}`}
              productType={item}
          />
      )
    })
  }
  
  return (
      <div className={'view-switcher'}>
        
        <Toolbar/>
        
        <div
            ref={elRef}
            className={'view-switcher__inner'}
            style={{width: `${numPanels * 100}%`, left: `-${currentViewSwitcherIndex * 100}%`}}>
          {renderProductLists()}
        </div>
      
      </div>
  )
}

export default ViewSwitcher;
